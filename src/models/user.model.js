const { Schema, model } = require("mongoose");

const userSchema = new Schema({
  firstName: {
    type: String,
    required: [true, "first name is required"],
  },

  lastName: {
    type: String,
    required: [true, "last name is required"],
  },

  mobile: {
    type: String,
    required: [true, "mobile number  is required"],
    unique: true,
  },

  email: {
    type: String,
    unique: true,
  },

  password: {
    type: String,
    required: [true, "password  is required"],
    min: 8,
    max: 10,
  },

  address: {
    houseNumber: {
      type: String,
    },
    street: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    state: {
      type: String,
      required: true,
    },
    pinCode: {
      type: Number,
      required: true,
    },
  },

  isDeleted: {
    type: Boolean,
    default: false,
  },
});

const User = model("User", userSchema);
module.exports = User;
