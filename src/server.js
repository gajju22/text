const dotenv = require("dotenv");
const { DataBase } = require("./config/db");

const app = require("./app");
const port = process.env.PORT || 3000;

dotenv.config();

//db connection;
DataBase();

//express--->
app.listen(port, () => console.log(`express is running on port ${port}`));
