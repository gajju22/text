const { Router } = require("express");
const router = Router();

const {
  register,
  updateProfile,
  deleteProfile,
  checkProfile,
} = require("../controllers/profileController");

const { signIn } = require("../controllers/sign-in");
const auth = require("../middleware/auth");

router.post("/sign-up", register);
router.post("/sign-in", signIn);
router.put("/:id/update", auth, updateProfile);
router.delete("/:id/delete", auth, deleteProfile);
router.get("/:id/profile", auth, checkProfile);

module.exports = router;
