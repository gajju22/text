const mongoose = require("mongoose");

exports.DataBase = async () => {
  try {
    const uri = process.env.DB_URI;
    console.log(uri);
    await mongoose.connect(uri);
    console.log("mongodb connected...");
  } catch (err) {
    console.log(err.message);
  }
};
