const jwt = require("jsonwebtoken");

const generateAuthToken = (payload, secKey) => {
  return jwt.sign(payload, secKey);
};

module.exports = generateAuthToken;
