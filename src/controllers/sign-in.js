"use strict";
const User = require("../models/user.model");
const generateAuthToken = require("../utils/genToken");

exports.signIn = async (req, res) => {
  try {
    const { id, password } = req.body;

    if (!id) {
      return res.status.send({
        status: false,
        message: "enter your number or email",
      });
    }

    if (!password) {
      return res.status.send({
        status: false,
        message: "enter valid password",
      });
    }

    const user = await User.findOne({ $or: [{ email: id }, { phone: id }] });

    if (!user) {
      return res
        .status(401)
        .send({ status: "unauthorized", message: "invalid credentials" });
    }

    if (user.isDeleted === true) {
      return res.status(404).send({ message: "user not found." });
    }

    if (user.password !== password) {
      return res
        .status(401)
        .send({ status: "unauthorized", message: "invalid credentials" });
    }

    const payload = { id: user._id };

    const token = generateAuthToken(payload, process.env.JWT_SEC_KEY);

    res.status(201).send({
      status: "success",
      message: `user ${user.firstName} successfully logged-in`,
      token: token,
    });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};
