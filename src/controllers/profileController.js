"use strict";
const User = require("../models/user.model");

//create user
exports.register = async (req, res) => {
  try {
    const { body } = req;
    const { firstName, lastName, mobile, password } = body;

    //checking if users profile already created-
    const user = await User.findOne({ mobile });

    if (!firstName) {
      return res
        .status(400)
        .send({ status: false, message: "firstName required" });
    }

    if (!lastName) {
      return res
        .status(400)
        .send({ status: false, message: "lastName required" });
    }

    if (!mobile) {
      return res
        .status(400)
        .send({ status: false, message: "mobile required" });
    }

    if (user) {
      return res
        .status(400)
        .send({ status: false, message: "already registered please log-in!" });
    }

    if (!password) {
      return res
        .status(400)
        .send({ status: false, message: "password required" });
    }

    if (password.length > 15 && password.length < 8) {
      return res
        .status(400)
        .send({ status: false, message: "enter valid password" });
    }

    const createdProfile = await User.create(body);
    res.status(201).send({ status: true, data: createdProfile });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};

//check users profile
exports.checkProfile = async (req, res) => {
  try {
    const { decode, params } = req;
    const { id } = params;

    if (decode.id !== id) {
      return res.status(403).send({ status: "not authorized" });
    }

    const user = await User.findById(id);

    if (!user || user.isDeleted === true) {
      return res.status(404).send({ status: false, message: "user not found" });
    }

    res.status(200).send({ stats: true, profile: user });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};

//update users profile
exports.updateProfile = async (req, res) => {
  try {
    const { body, params, decode } = req;
    const { id } = params;
    const { firstName, lastName, mobile, email } = body;

    const userProperties = {};

    if (decode.id !== id) {
      return res.status(403).send({ status: "not authorized" });
    }

    if (firstName) {
      userProperties["firstName"] = firstName;
    }

    if (lastName) {
      userProperties["lastName"] = lastName;
    }

    if (mobile) {
      userProperties["mobile"] = mobile;
    }

    if (email) {
      userProperties["email"] = email;
    }

    if (Object.keys(body).length === 0) {
      return res
        .status(400)
        .send({ status: false, message: "enter filled to be update." });
    }

    const updatedProfile = await User.findOneAndUpdate(
      { _id: id, isDeleted: false },
      { $set: userProperties },
      { new: true }
    );

    if (!updatedProfile) {
      return res
        .status(404)
        .send({ status: false, message: "user not found." });
    }

    res.status(200).send({
      status: "profile successfully updated",
      profile: updatedProfile,
    });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};

//delete users profile
exports.deleteProfile = async (req, res) => {
  try {
    const { id } = req.params;

    if (req.decode.id !== id) {
      return res.status(403).send({ status: "not authorized" });
    }

    const user = await User.findById(id);

    if (!user || user.isDeleted === true) {
      return res
        .status(404)
        .send({ status: false, message: "user already deleted" });
    }

    await User.findByIdAndUpdate(id, { $set: { isDeleted: true } });
    res.status(200).send({ message: "user's profile successfully deleted" });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};
