"use strict";
const jwt = require("jsonwebtoken");

const auth = async (req, res, next) => {
  try {
    const { authorization } = req.headers;
    if (authorization && authorization.startsWith("Bearer")) {
      const token = authorization.split(" ")[1];
      if (!token) {
        res.status(403).send({ status: "Not authorized" });
      }

      const decode = jwt.verify(token, process.env.JWT_SEC_KEY);
      if (!decode) {
        return res.status(403).send({ status: "unauthorized" });
      }

      req.decode = decode;
      next();
    } else {
      return res.status(403).send({ status: "Not authorized" });
    }
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};

module.exports = auth;
